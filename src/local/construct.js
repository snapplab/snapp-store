let {Directory} = global.SnappFramework;

module.exports = function (self) {
  self.path = this.optional.Directory || Directory('/var/db/snapp-framework');
  self.path = self.path.directory(this.Namespace.value);
};
