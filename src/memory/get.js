module.exports = function (self) {
  let name = this.string;
  let miss = this.function;
  if (!name in self.items) {
    self.items[name] = miss();
  }
  return self.items[name];
};
